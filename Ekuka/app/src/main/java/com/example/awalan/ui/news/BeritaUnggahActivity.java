package com.example.android.e_kuka.ui.news;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.example.android.e_kuka.MainActivity;
import com.example.android.e_kuka.R;
import com.example.android.e_kuka.Resep.ResepUnggahActivity;

public class BeritaUnggahActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_berita_unggah);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                Intent i = new Intent(BeritaUnggahActivity.this, MainActivity.class);
                startActivity(i);

                finish();
            }
        }, 1000);
    }
}
