package com.example.android.e_kuka.ui.news;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android.e_kuka.Adapter.BeritaAdapter;
import com.example.android.e_kuka.Lomba.TambahLombaActivity;
import com.example.android.e_kuka.Model.Berita;
import com.example.android.e_kuka.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

public class NewsFragment extends Fragment {

    DatabaseReference databaseReference;

    FloatingActionButton fabNews;
    RecyclerView rvNews;
    BeritaAdapter bAdapter;
    List<Berita> beritaList = new ArrayList<>();

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        databaseReference = FirebaseDatabase.getInstance().getReference();

        View root = inflater.inflate(R.layout.fragment_news, container, false);
        fabNews = root.findViewById(R.id.fabNews);
        fabNews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), BeritaGambarActivity.class));
            }
        });

        rvNews = root.findViewById(R.id.rvNews);
        rvNews.setLayoutManager(new LinearLayoutManager(getActivity()));
        bAdapter = new BeritaAdapter(getActivity(), beritaList);
        rvNews.setAdapter(bAdapter);

        return root;
    }

    @Override
    public void onStart() {
        super.onStart();
        databaseReference.child("berita").addChildEventListener(listener);
    }

    @Override
    public void onStop() {
        super.onStop();
        databaseReference.child("berita").removeEventListener(listener);
    }

    private ChildEventListener listener = new ChildEventListener() {
        @Override
        public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
            Berita berita = dataSnapshot.getValue(Berita.class);
            beritaList.add(berita);
            bAdapter.notifyItemInserted(bAdapter.getItemCount() - 1);
        }

        @Override
        public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

        }

        @Override
        public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

        }

        @Override
        public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {

        }
    };
}
