package com.example.android.e_kuka.ui.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android.e_kuka.Adapter.ResepAdapter;
import com.example.android.e_kuka.Model.Resep;
import com.example.android.e_kuka.R;
import com.example.android.e_kuka.RecyclerViewClickListener;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class HomeFragment extends Fragment {

    private RecyclerView rvHome;
    private ResepAdapter rAdapter;
    private List<Resep> resepList = new ArrayList<>();
    List<String> keyList = new ArrayList<>();
    private DatabaseReference databaseReference;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        rvHome = root.findViewById(R.id.rvHome);
        rvHome.setLayoutManager(new LinearLayoutManager(getActivity()));
        rAdapter = new ResepAdapter(getActivity(), resepList, new RecyclerViewClickListener() {
            @Override
            public void onCardClick(View v, int pos) {

            }
        });
        rvHome.setAdapter(rAdapter);

        databaseReference = FirebaseDatabase.getInstance().getReference();
        return root;
    }

    @Override
    public void onStart() {
        super.onStart();
        keyList.clear();
        resepList.clear();
        databaseReference.child("resep").addChildEventListener(listener);
    }

    @Override
    public void onStop() {
        super.onStop();
        databaseReference.child("resep").removeEventListener(listener);
    }

    ChildEventListener listener = new ChildEventListener() {
        @Override
        public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
            Resep resep = dataSnapshot.getValue(Resep.class);
            keyList.add(resep.getUuid());
            resepList.add(resep);

            rAdapter.notifyItemInserted(rAdapter.getItemCount() - 1);
        }

        @Override
        public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

        }

        @Override
        public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
            int pos = keyList.indexOf(dataSnapshot.getKey());
            if (pos > -1) {
                keyList.remove(pos);
                resepList.remove(pos);

                rAdapter.notifyItemRemoved(pos);
            }
        }

        @Override
        public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {

        }
    };
}
