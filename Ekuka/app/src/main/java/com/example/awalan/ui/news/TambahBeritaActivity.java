package com.example.android.e_kuka.ui.news;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import com.example.android.e_kuka.EditTextValidation;
import com.example.android.e_kuka.Model.Berita;
import com.example.android.e_kuka.Model.Resep;
import com.example.android.e_kuka.ModelToMap;
import com.example.android.e_kuka.R;
import com.example.android.e_kuka.Resep.ResepActivity;
import com.example.android.e_kuka.Resep.ResepUnggahActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TambahBeritaActivity extends AppCompatActivity {

    EditText etJudulBerita, etIsiBerita;
    String uri;
    DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_berita);

        etJudulBerita = findViewById(R.id.etJudulBerita);
        etIsiBerita = findViewById(R.id.etIsiBerita);
        databaseReference = FirebaseDatabase.getInstance().getReference();

        uri = getIntent().hasExtra("uri") ? getIntent().getStringExtra("uri") : null;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        List<EditText> etList = new ArrayList<>();
        etList.add(etJudulBerita);
        etList.add(etIsiBerita);

        if(!EditTextValidation.isEmpty(etList)) {
            String key = databaseReference.child("berita").push().getKey();
            Berita berita = new Berita(
                    key,
                    etJudulBerita.getText().toString(),
                    etIsiBerita.getText().toString(),
                    uri
            );

            Map<String, Object> values = ModelToMap.toMap(berita);

            Map<String, Object> childUpdates = new HashMap<>();
            childUpdates.put("berita/"+ key,values);

            databaseReference.updateChildren(childUpdates);

            startActivity(new Intent(TambahBeritaActivity.this, BeritaUnggahActivity.class));
        }

        return super.onOptionsItemSelected(item);
    }
}
