package com.example.android.e_kuka;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Map;

public  class ModelToMap {
    public static Map toMap(Object obj) {
        Gson gson = new GsonBuilder().create();
        String json = gson.toJson(obj);
        Map<String, Object> map = new Gson().fromJson(json, Map.class);

        return map;
    }
}
