package com.example.android.e_kuka;

import android.view.View;

public interface RecyclerViewClickListener {
    void onCardClick(View v, int pos);
}
