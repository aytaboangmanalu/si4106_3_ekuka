package com.example.android.e_kuka;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;
import java.util.List;

public class MasukActivity extends AppCompatActivity {

    EditText etEmailMasuk, etPasswordMasuk;
    FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_masuk);

        etEmailMasuk = findViewById(R.id.etEmailMasuk);
        etPasswordMasuk = findViewById(R.id.etPasswordMasuk);

        mAuth = FirebaseAuth.getInstance();
    }

    public void masuk(View view) {
        List<EditText> etList = new ArrayList<>();
        etList.add(etEmailMasuk);
        etList.add(etPasswordMasuk);

        if (EditTextValidation.isEmpty(etList)) {
            mAuth.signInWithEmailAndPassword(etEmailMasuk.getText().toString(),etPasswordMasuk.getText().toString())
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                startActivity(new Intent(MasukActivity.this, MainActivity.class));
                                finish();
                            } else {
                                Toast.makeText(MasukActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        }

    }
}
